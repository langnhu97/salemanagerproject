﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCPhanQuyen : DevExpress.XtraEditors.XtraUserControl
    {
        public UCPhanQuyen()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void UCPhanQuyen_Load(object sender, EventArgs e)
        {
            cbTaiKhoan.DataSource = da.GetSqlTable("SELECT TaiKhoan FROM NguoiDung");
            cbTaiKhoan.DisplayMember = "TaiKhoan";
            cbTaiKhoan.ValueMember = "TaiKhoan";

            cbCVMoi.DataSource = da.GetSqlTable("SELECT MaChucVu, TenChucVu FROM LoaiChucVu");
            cbCVMoi.DisplayMember = "TenChucVu";
            cbCVMoi.ValueMember = "MaChucVu";
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            string taikhoan = cbTaiKhoan.SelectedValue.ToString();
            int machucvu = int.Parse(cbCVMoi.SelectedValue.ToString());
            string sql = $"UPDATE NguoiDung SET MaChucVu = {machucvu} where TaiKhoan LIKE '{taikhoan}'";

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã phân quyền thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công", "Thông báo");
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
        }
    }
}
