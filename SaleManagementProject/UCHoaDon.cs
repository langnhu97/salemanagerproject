﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCHoaDon : DevExpress.XtraEditors.XtraUserControl
    {
        public UCHoaDon()
        {
            InitializeComponent();
        }
        
        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvHoaDon.DataSource = da.GetSqlTable("SELECT * FROM HoaDon");
            dgvSanPham.DataSource = da.GetSqlTable("SELECT MaSP, TenSP, SoLuong, DonGia FROM SanPham");
        }

        private void UCHoaDon_Load(object sender, EventArgs e)
        {
            cbMaKH.DataSource = da.GetSqlTable("SELECT MaKH, TenKH FROM KhachHang");
            cbMaKH.DisplayMember = "TenKH";
            cbMaKH.ValueMember = "MaKH";

            cbMaNV.DataSource = da.GetSqlTable("SELECT MaNV, HoTenNV FROM NhanVien");
            cbMaNV.DisplayMember = "HoTenNV";
            cbMaNV.ValueMember = "MaNV";

            HienThiDL();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int makh = Convert.ToInt32(cbMaKH.SelectedValue.ToString());
            string manv = cbMaNV.SelectedValue.ToString();
            string masp = txtSanPham.Text;
            DateTime ngayban = new DateTime(dtpNgayGiao.Value.Year, dtpNgayGiao.Value.Month, dtpNgayGiao.Value.Day);
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float giaban = (txtGiaBan.Text != "") ? Convert.ToSingle(txtGiaBan.Text) : 0f;

            string sql = $"INSERT INTO HoaDon(MaKH,MaNV,MaSP,NgayBan,SoLuong,GiaBan,ThanhTien)" +
                $"VALUES ({makh},'{manv}','{masp}','{ngayban}',{soluong},{giaban},{soluong}*{giaban})";
            string addSpQuery = $"UPDATE SanPham SET SoLuong = SoLuong - {soluong} WHERE MaSP LIKE '{masp}'";

            if (da.ExecuteNonQueryCmd(sql) > 0 && da.ExecuteNonQueryCmd(addSpQuery) > 0)
            {
                MessageBox.Show("Đã thêm DL thành công");

            }
            else
            {
                MessageBox.Show("KHÔNG thành công");
            }
            HienThiDL();
        }


        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string tensp = txtTimKiem.Text;
                string sql = $"SELECT MaSP, TenSP, SoLuong FROM SanPham WHERE tensp LIKE '%{tensp}%'";
                dgvSanPham.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvHoaDon.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM HoaDon WHERE MaHD LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void dgvHoaDon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvSanPham_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtSanPham.Text = dgvSanPham.CurrentRow.Cells["MaSP"].Value.ToString();
        }

        private void dgvSanPham_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtSanPham.Text = dgvSanPham.CurrentRow.Cells["MaSP"].Value.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }
    }
}
