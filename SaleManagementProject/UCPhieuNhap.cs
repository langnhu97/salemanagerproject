﻿using DevExpress.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCPhieuNhap : UserControl
    {
        public UCPhieuNhap()
        {
            InitializeComponent();
        }

        //private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        //{

        //}

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvPhieuNhap.DataSource = da.GetSqlTable("SELECT * FROM PhieuNhap");
            dgvSanPham.DataSource = da.GetSqlTable("SELECT MaSP, TenSP, SoLuong, DonGia FROM SanPham");
        }
        private void UCPhieuNhap_Load(object sender, EventArgs e)
        {
            cbMaNCC.DataSource = da.GetSqlTable("SELECT MaNCC, TenNCC FROM NhaCungCap");
            cbMaNCC.DisplayMember = "TenNCC";
            cbMaNCC.ValueMember = "MaNCC";

            cbMaNV.DataSource = da.GetSqlTable("SELECT MaNV, HoTenNV FROM NhanVien");
            cbMaNV.DisplayMember = "HoTenNV";
            cbMaNV.ValueMember = "MaNV";

            HienThiDL();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string tensp = txtTimKiem.Text;
                string sql = $"SELECT MaSP, TenSP, SoLuong FROM SanPham WHERE tensp LIKE '%{tensp}%'";
                dgvSanPham.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void dgvSanPham_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtSanPham.Text = dgvSanPham.CurrentRow.Cells["MaSP"].Value.ToString();
        }

        private void dgvSanPham_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtSanPham.Text = dgvSanPham.CurrentRow.Cells["MaSP"].Value.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string mancc = cbMaNCC.SelectedValue.ToString();
            string masp = txtSanPham.Text;
            DateTime ngaygiao = new DateTime(dtpNgayGiao.Value.Year, dtpNgayGiao.Value.Month, dtpNgayGiao.Value.Day);
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float dongia = (txtDonGia.Text != "") ? Convert.ToSingle(txtDonGia.Text) : 0f;
            string manv = cbMaNV.SelectedValue.ToString();

            //string sql = $"INSERT INTO PhieuNhap(MaNCC,MaSP,NgayGiao,SoLuong,DonGia,ThanhTien,MaNV)" +
            //    $"VALUES ({mancc},'{masp}','{ngaygiao}',{soluong},{donGia},{soluong}*{donGia},'{manv}')";

            string sql = $"INSERT INTO PhieuNhap(MaNCC,MaSP,NgayGiao,SoLuong,DonGia,ThanhTien,MaNV)" +
                $" VALUES('{mancc}','{masp}','{ngaygiao}',{soluong},{dongia},{soluong}*{dongia},'{manv}')";

            string addSpQuery = $"UPDATE SanPham SET SoLuong = SoLuong + {soluong} WHERE MaSP LIKE '{masp}'";

            if (da.ExecuteNonQueryCmd(sql) > 0 && da.ExecuteNonQueryCmd(addSpQuery) > 0)
            {
                MessageBox.Show("Đã thêm DL thành công");

            }
            else
            {
                MessageBox.Show("KHÔNG thành công");
            }
            HienThiDL();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvPhieuNhap.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM PhieuNhap WHERE MaPN LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }
    }
}
