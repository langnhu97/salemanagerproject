﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCNhaCungCap : DevExpress.XtraEditors.XtraUserControl
    {
        public UCNhaCungCap()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvNCC.DataSource = da.GetSqlTable("SELECT * FROM NhaCungCap");
        }
        private void UCNhaCungCap_Load(object sender, EventArgs e)
        {
            HienThiDL();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string tentimkiem = txtTimKiem.Text;
                string sql = $"SELECT * FROM NhaCungCap WHERE TenNCC LIKE N'%{tentimkiem}%'";
                dgvNCC.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvNCC.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM NhaCungCap WHERE MaNCC LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string mancc = txtMaNCC.Text;
            string tenncc = txtTenNCC.Text;
            string tengiaodich = txtTenGiaoDich.Text;
            string diachi = txtDiaChi.Text;
            string dienthoai = txtDienThoai.Text;
            string email = txtEmail.Text;

            string sql = $"INSERT INTO NhaCungCap(MaNCC,TenNCC,TenGiaoDich,DiaChi,DienThoai,Email)" +
                $"VALUES ('{mancc}',N'{tenncc}',N'{tengiaodich}',N'{diachi}','{dienthoai}','{email}')";

            if(da.ExecuteNonQueryCmd(sql) > 0){
                MessageBox.Show("Đã thêm DL thành công");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công");
            }
            HienThiDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string mancc = txtMaNCC.Text;
            string tenncc = txtTenNCC.Text;
            string tengiaodich = txtTenGiaoDich.Text;
            string diachi = txtDiaChi.Text;
            string dienthoai = txtDienThoai.Text;
            string email = txtEmail.Text;


            string sql = $"UPDATE NhaCungCap SET MaNCC='{mancc}', TenNCC=N'{tenncc}', TenGiaoDich=N'{tengiaodich}'," +
                $"DiaChi=N'{diachi}', DienThoai='{dienthoai}', Email=N'{email}' WHERE MaNCC LIKE '{mancc}'";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void dgvNCC_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMaNCC.Text = dgvNCC.CurrentRow.Cells[0].Value.ToString();
            txtTenNCC.Text = dgvNCC.CurrentRow.Cells[1].Value.ToString();
            txtTenGiaoDich.Text = dgvNCC.CurrentRow.Cells[2].Value.ToString();
            txtDiaChi.Text = dgvNCC.CurrentRow.Cells[3].Value.ToString();
            txtDienThoai.Text = dgvNCC.CurrentRow.Cells[4].Value.ToString();
            txtEmail.Text = dgvNCC.CurrentRow.Cells[5].Value.ToString();
        }
    }
}
