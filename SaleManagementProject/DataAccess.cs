﻿using DevExpress.XtraScheduler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    class DataAccess //Chứa các thuộc tính dùng chung cho ứng dụng
    {
        public SqlConnection Conn = null;
        public DataAccess()
        {
            //Lấy tên chuỗi từ tệp App.Config
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["QLBHConnStr"].ToString();
            //Khởi tạo một đối tượng kết nối DB
            Conn = new SqlConnection(cs);            
        }

        public DataTable GetSqlTable(string sQuery)
        {
            DataTable dt = new DataTable();
            //if(Conn.State == ConnectionState.Open)
            //{
            //    Conn.Close();
            //}
            //else
            //{
            //    Conn.Open();
            //}

           
            try
            {
                Conn.Open();
                Console.WriteLine("Kết nối DB thành công");
                
                SqlDataAdapter sda = new SqlDataAdapter(sQuery, Conn);
                sda.Fill(dt);   //Add các row từ bảng trong DB vào đối tượng DataTable
            
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                dt = null;
            }
            finally
            {
                Conn.Close();
            }
            
            return dt;
        }
        public int ExecuteNonQueryCmd(string sQuery)
        {
            try
            {
                int n = 0;
                if(Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
                else
                {
                    Conn.Open();
                    SqlCommand cmd = new SqlCommand(sQuery, Conn);
                    n = cmd.ExecuteNonQuery();
                    return n;
                }
            }
            catch ( SqlException e)
            {
                Console.WriteLine(e);               
            }
            finally
            {
                Conn.Close();
            }
            return -1;
        }

        public List<NguoiDung> FindAllUser()
        {
            List<NguoiDung> DSNguoiDung = new List<NguoiDung>();
            string sql = "SELECT * FROM NguoiDung";
            try
            {
                Conn.Open();
                SqlCommand cmd = new SqlCommand(sql, Conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string taikhoan = reader.GetString(0);
                            string matkhau = reader.GetString(1);
                            int machucvu = reader.GetInt32(2);
                            DSNguoiDung.Add(new NguoiDung(taikhoan, matkhau, machucvu));
                        }
                    }
                }
            }
            catch(SqlException e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
            finally
            {
                Conn.Close();
            }
                       
            return DSNguoiDung;
        }


    }

    class NguoiDung
    {
        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public int MaChucVu { get; set; }

        public NguoiDung()
        {

        }

        public NguoiDung(string taikhoan, string matkhau)
        {
            TaiKhoan = taikhoan;
            MatKhau = matkhau;
        }

        public NguoiDung(string taikhoan, string matkhau, int machucvu)
        {
            TaiKhoan = taikhoan;
            MatKhau = matkhau;
            MaChucVu = machucvu;
        }
    }
}