﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCBaoCao : DevExpress.XtraEditors.XtraUserControl
    {
        public UCBaoCao()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        private void labelControl1_Click(object sender, EventArgs e)
        {
            cbNam.DataSource = da.GetSqlTable("SELECT YEAR(NgayGiao) FROM PhieuNhap UNION SELECT YEAR");
        }

        private void UCBaoCao_Load(object sender, EventArgs e)
        {
            cbNam.DataSource = da.GetSqlTable("SELECT YEAR(NgayBan) AS Nam FROM HoaDon UNION SELECT YEAR(NgayGiao) AS Nam FROM PhieuNhap");
            cbNam.ValueMember = "Nam";
            cbNam.DisplayMember = "Nam";
            cbThang.DataSource = da.GetSqlTable("SELECT MONTH(NgayBan) AS Thang FROM HoaDon UNION SELECT MONTH(NgayGiao) AS Thang FROM PhieuNhap");
            cbThang.ValueMember = "Thang";
            cbThang.DisplayMember = "Thang";            
        }

        private void cbThang_DisplayMemberChanged(object sender, EventArgs e)
        {
            
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            int nam = int.Parse(cbNam.SelectedValue.ToString());
            int thang = int.Parse(cbThang.SelectedValue.ToString());

            //Thống kê Phiếu nhập trong tháng
            DataTable view_ThongKePhieuNhap = da.GetSqlTable($"CREATE OR ALTER VIEW view_ThongKePhieuNhap AS " +
                $"SELECT TenSP, TenNCC, NgayGiao, sp.SoLuong, sp.DonGia, ThanhTien FROM PhieuNhap pn " +
                $"JOIN NhaCungCap ncc ON ncc.MaNCC = pn.MaNCC " +
                $"JOIN  SANPHAM sp ON sp.MaSP = pn.MaSP " +
                $"WHERE YEAR(NgayGiao) = {nam} AND MONTH(NgayGiao) = {thang}");
            dgvPhieuNhap.DataSource = da.GetSqlTable($"SELECT * FROM view_ThongKePhieuNhap");

            float tongTienPN = 0;
            int soPN = dgvPhieuNhap.RowCount;
            for (int i = 0; i < soPN - 1; i++)
            {
                tongTienPN += float.Parse(dgvPhieuNhap.Rows[i].Cells["ThanhTien"].Value.ToString());
            }

            txtTongTienPN.Text = tongTienPN.ToString();

            //Thống kê Hóa đơn trong tháng
            
            DataTable view_ThongKeHoaDon = da.GetSqlTable($"CREATE OR ALTER VIEW view_ThongKeHoaDon AS " +
                $" SELECT TenSP, TenKH, NgayBan, hd.SoLuong, hd.GiaBan, ThanhTien FROM HoaDon hd " +
                $"JOIN KhachHang kh ON kh.MaKH = hd.MaKH " +
                $"JOIN  SANPHAM sp ON sp.MaSP = hd.MaSP " +
                $"WHERE YEAR(NgayBan) = {nam} AND MONTH(NgayBan) = {thang}");
            dgvHoaDon.DataSource = da.GetSqlTable("SELECT * FROM View_ThongKeHoaDon");

            float tongTienHD = 0;
            int soHD = dgvHoaDon.RowCount;
            for (int i = 0; i < soHD - 1; i++)
            {
                tongTienHD += float.Parse(dgvHoaDon.Rows[i].Cells["ThanhTien"].Value.ToString());
            }

            txtTongTienHD.Text = tongTienHD.ToString();
        }
    }
}