﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCNhanVien : DevExpress.XtraEditors.XtraUserControl
    {
        public UCNhanVien()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvNhanVien.DataSource = da.GetSqlTable("SELECT * FROM NhanVien");
        }

        private void UCNhanVien_Load(object sender, EventArgs e)
        {
            HienThiDL();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                string tentimkiem = txtTimKiem.Text;
                string sql = $"SELECT * FROM NhanVien WHERE TenNV LIKE N'%{tentimkiem}%'";
                dgvNhanVien.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string manv = txtMaNV.Text;
            string tennv = txtTenNV.Text;
            DateTime ngaysinh = new DateTime(dtpNgaySinh.Value.Year, dtpNgaySinh.Value.Month, dtpNgaySinh.Value.Day);
            string diachi = txtDiaChi.Text;
            string dienthoai = txtDienThoai.Text;
            float luong = (txtLuong.Text != "") ? Convert.ToSingle(txtLuong.Text) : 0f;

            string sql = $"INSERT INTO NhanVien(MaNV,HoTenNV,NgaySinh,DiaChi,DienThoai,LuongCoBan)" +
                $"VALUES ('{manv}',N'{tennv}','{ngaysinh}',N'{diachi}','{dienthoai}',{luong})";

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã thêm DL thành công");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công");
            }
            HienThiDL();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvNhanVien.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM NhanVien WHERE MaNV LIKE '{ma}'";
            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã xóa thành công DL", "Thông báo");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công", "Thông báo");
            }
            HienThiDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string manv = txtMaNV.Text;
            string tennv = txtTenNV.Text;
            DateTime ngaysinh = new DateTime(dtpNgaySinh.Value.Year, dtpNgaySinh.Value.Month, dtpNgaySinh.Value.Day);
            string diachi = txtDiaChi.Text;
            string dienthoai = txtDienThoai.Text;
            float luong = (txtLuong.Text != "") ? Convert.ToSingle(txtLuong.Text) : 0f;

            string sql = $"UPDATE NhanVien SET MaNV = '{manv}',HoTenNV = N'{tennv}',NgaySinh = '{ngaysinh}'," +
                $"DiaChi = N'{diachi}',DienThoai = '{dienthoai}',LuongCoBan = {luong} WHERE MaNV LIKE '{manv}'";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL(); ;
        }

        private void dgvNhanVien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMaNV.Text = dgvNhanVien.CurrentRow.Cells["MaNV"].Value.ToString();
            txtTenNV.Text = dgvNhanVien.CurrentRow.Cells["HoTenNV"].Value.ToString();
            dtpNgaySinh.Value = Convert.ToDateTime(dgvNhanVien.CurrentRow.Cells["NgaySinh"].Value.ToString());
            txtDiaChi.Text = dgvNhanVien.CurrentRow.Cells["DiaChi"].Value.ToString();
            txtDienThoai.Text = dgvNhanVien.CurrentRow.Cells["DienThoai"].Value.ToString();
            txtLuong.Text = dgvNhanVien.CurrentRow.Cells["LuongCoBan"].Value.ToString();
        }
    }
}
