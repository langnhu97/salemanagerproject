﻿using DevExpress.Portable;
using DevExpress.XtraScheduler.VCalendar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class Login : Form
    {
        public string TkLogin{ get; set; }

        public string MkLogin { get; set; }

        public string CvLogin { get; set; }
        public Login()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();
        List<NguoiDung> DSNguoiDung = new List<NguoiDung>();
        private void labelControl2_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {
            DSNguoiDung = da.FindAllUser();
            
        }

        void LoadMainForm()
        {
            Form1 frm1 = new Form1();
            frm1.ShowDialog();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            bool checkAcc = false;
            bool checkPass = false;
            foreach (NguoiDung ngd in DSNguoiDung)
            {
                if(ngd.TaiKhoan == txtUsername.Text)
                {
                    checkAcc = true;
                    if(ngd.MatKhau == txtPassword.Text)
                    {
                        checkPass = true;
                        LoginInfo.TKLogin = ngd.TaiKhoan;
                        LoginInfo.MKLogin = ngd.MatKhau;
                        LoginInfo.CVLogin = ngd.MaChucVu;
                    }
                }
            }
            if(checkAcc && checkPass)
            {
                LoadMainForm();
            }
            else
            {
                MessageBox.Show("Thông tin đăng nhập không đúng");
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    static class LoginInfo
    {
        public static string TKLogin { get; set; }
        public static string MKLogin { get; set; }
        public static int CVLogin { get; set; }
    }
}
