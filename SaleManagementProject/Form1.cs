﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Login lg = new Login();
        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void navBarControl1_Click_1(object sender, EventArgs e)
        {
            //frmKhachHang fKH = new frmKhachHang();
            //fKH.ShowDialog();

        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCKhachHang uckh = new UCKhachHang();
            uckh.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(uckh);
        }

        private void navBarItem2_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCSanPham ucsp = new UCSanPham();
            ucsp.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(ucsp);
        }

        private void navBarItem4_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCNhaCungCap ucncc = new UCNhaCungCap();
            ucncc.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(ucncc);
        }

        private void navBarItem8_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCNhanVien ucnv = new UCNhanVien();
            ucnv.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(ucnv);
        }

        private void navBarItem9_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCNhomSanPham uchd = new UCNhomSanPham();
            uchd.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(uchd);
        }

        private void navBarItem5_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
           
            if(LoginInfo.CVLogin == 1)
            {
                splitContainer1.Panel2.Controls.Clear();
                UCNguoiDung ucnd = new UCNguoiDung();
                ucnd.Dock = DockStyle.Fill;
                splitContainer1.Panel2.Controls.Add(ucnd);
            }
            else
            {
                MessageBox.Show($"Chỉ Admin mới có quyền truy cập chức năng này", "Thông báo");
            }
        }

        private void navBarItem3_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (LoginInfo.CVLogin == 1)
            {
                splitContainer1.Panel2.Controls.Clear();
                UCPhanQuyen ucpq = new UCPhanQuyen();
                ucpq.Dock = DockStyle.Fill;
                splitContainer1.Panel2.Controls.Add(ucpq);
            }
            else
            {
                MessageBox.Show($"Chỉ Admin mới có quyền truy cập chức năng này", "Thông báo");
            }
        }

        private void navBarItem6_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCDoiMatKhau ucdmk = new UCDoiMatKhau();
            ucdmk.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(ucdmk);
        }

        private void navBarItem10_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCHoaDon uchd = new UCHoaDon();
            uchd.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(uchd);
        }

        private void navBarItem7_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCPhieuNhap ucpn = new UCPhieuNhap();
            ucpn.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(ucpn);
        }

        private void navBarItem11_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            splitContainer1.Panel2.Controls.Clear();
            UCBaoCao ucbc = new UCBaoCao();
            ucbc.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Add(ucbc);
        }
    }
}
