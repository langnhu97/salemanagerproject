﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLy
{
    public partial class UCDoiMatKhau : DevExpress.XtraEditors.XtraUserControl
    {
        public UCDoiMatKhau()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
        }

        private void UCDoiMatKhau_Load(object sender, EventArgs e)
        {
            txtTaiKhoan.Text = LoginInfo.TKLogin;
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            string taikhoan = LoginInfo.TKLogin;
            string matkhau = txtMatKhau.Text;
            string sql = $"UPDATE NguoiDung SET MatKhau = '{matkhau}' WHERE TaiKhoan LIKE '{taikhoan}'";

            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                MessageBox.Show("Đã đổi mật thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("KHÔNG thành công", "Thông báo");
            }
        }
    }
}
