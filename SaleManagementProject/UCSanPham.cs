﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Mask.Design;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace PhanMemQuanLy
{
    public partial class UCSanPham : DevExpress.XtraEditors.XtraUserControl
    {
        public UCSanPham()
        {
            InitializeComponent();
        }

        DataAccess da = new DataAccess();

        void HienThiDL()
        {
            dgvSanPham.DataSource = da.GetSqlTable("SELECT * FROM SanPham");
        }

        private void UCSanPham_Load(object sender, EventArgs e)
        {
            cbMaNhom.DataSource = da.GetSqlTable("SELECT MaNhom, TenNhom FROM NhomSanPham");
            cbMaNhom.ValueMember = "MaNhom";
            cbMaNhom.DisplayMember = "TenNhom";

            cbMaNCC.DataSource = da.GetSqlTable("SELECT MaNCC, TenNCC FROM NhaCungCap");
            cbMaNCC.ValueMember = "MaNCC";
            cbMaNCC.DisplayMember = "TenNCC";
            HienThiDL();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string masp = txtMaSp.Text;
            string tensp = txtTenSp.Text;
            string manhom = cbMaNhom.SelectedValue.ToString();
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float dongia = (txtDonGia.Text != "") ? Convert.ToSingle(txtDonGia.Text) : 0f;
            string mancc = cbMaNCC.SelectedValue.ToString();

            string sql = $"INSERT INTO SanPham([MaSp],[TenSp],[MaNhom],[SoLuong],[DonGia],[MaNCC]) VALUES" +
                $"('{masp}',N'{tensp}','{manhom}',{soluong},{dongia},'{mancc}')";


            if (da.ExecuteNonQueryCmd(sql) > 0)
            {
                Console.WriteLine("Thêm thông tin sản phẩm thành công", "Thông báo");
            }
            else
            {
                Console.WriteLine("KHÔNG thêm được thông tin sản phẩm", "Thông báo");
            }
            HienThiDL();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if(txtTimKiem.Text != "")
            {
                string tentimkiem = txtTimKiem.Text;
                string sql = $"SELECT * FROM SanPham WHERE TenSp LIKE N'%{tentimkiem}%'";
                dgvSanPham.DataSource = da.GetSqlTable(sql);
            }
            else
            {
                HienThiDL();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string ma = dgvSanPham.SelectedRows[0].Cells[0].Value.ToString();
            string sql = $"DELETE FROM SanPham WHERE MaSP LIKE '{ma}'";
            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel2.Controls.Clear();
        }

        private void dgvSanPham_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    txtMaSp.Text = dgvSanPham.CurrentRow.Cells["MaSp"].Value.ToString();
            //    txtTenSp.Text = dgvSanPham.CurrentRow.Cells["TenSp"].Value.ToString();
            //    cbMaNhom.ValueMember = dgvSanPham.CurrentRow.Cells["MaNhom"].Value.ToString();
            //    txtSoLuong.Text = dgvSanPham.CurrentRow.Cells["SoLuong"].Value.ToString();
            //    txtDonGia.Text = dgvSanPham.CurrentRow.Cells["DonGia"].Value.ToString();
            //    cbMaNCC.ValueMember = dgvSanPham.CurrentRow.Cells["MaNCC"].Value.ToString();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);   
            //}
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string masp = txtMaSp.Text;
            string tensp = txtTenSp.Text;
            string manhom = cbMaNhom.SelectedValue.ToString();
            int soluong = (txtSoLuong.Text != "") ? Convert.ToInt32(txtSoLuong.Text) : 0;
            float dongia = (txtDonGia.Text != "") ? Convert.ToSingle(txtDonGia.Text) : 0f;
            string mancc = cbMaNCC.SelectedValue.ToString();


            string sql = $"UPDATE SanPham SET MaSP='{masp}', TenSp=N'{tensp}', MaNhom=N'{manhom}'," +
                $"SoLuong={soluong}, DonGia={dongia}, MaNCC='{mancc}' WHERE MaSP LIKE '{masp}'";

            DialogResult dlr = MessageBox.Show("Bạn chắc chẳn thay đổi?", "Thông báo",
               MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (da.ExecuteNonQueryCmd(sql) > 0)
                {
                    MessageBox.Show("Đã sửa DL thành công!", "Thông báo!");
                }
                else
                {
                    MessageBox.Show("Chưa sửa được DL", "Thông báo");
                }
            }
            HienThiDL();
        }

        private void dgvSanPham_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtMaSp.Text = dgvSanPham.CurrentRow.Cells["MaSP"].Value.ToString();
                txtTenSp.Text = dgvSanPham.CurrentRow.Cells["TenSp"].Value.ToString();
                cbMaNhom.SelectedValue = dgvSanPham.CurrentRow.Cells["MaNhom"].Value.ToString();
                txtSoLuong.Text = dgvSanPham.CurrentRow.Cells["SoLuong"].Value.ToString();
                txtDonGia.Text = dgvSanPham.CurrentRow.Cells["DonGia"].Value.ToString();
                cbMaNCC.SelectedValue = dgvSanPham.CurrentRow.Cells["MaNCC"].Value.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
